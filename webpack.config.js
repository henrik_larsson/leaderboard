const webpack = require('webpack')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
  devtool: 'source-map',

  entry: './client/',

  output: {
    path: __dirname + '/__build__',
    filename: 'build.js',
    publicPath: '__build__'
  },

  resolve: {
    extensions: ['.js', '.css']
  },

  module: {
    loaders: [
      {test: /\.css$/, loader: 'style!css'},
      {test: /\.js$/, exclude: /node_modules|mocha-browser\.js/, loader: 'babel-loader'},
      {test: /\.woff(2)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
      {test: /\.ttf$/, loader: 'file'},
      {test: /\.eot$/, loader: 'file'},
      {test: /\.svg$/, loader: 'file'}
    ]
  },

  // plugins: [
  //     new webpack.optimize.CommonsChunkPlugin({ name: 'shared' })
  // ],

  devServer: {
    quiet: false,
    noInfo: false,
    historyApiFallback: {
      rewrites: [
        {
          from: /ReduxDataFlow\/exercise.html/,
          to: 'ReduxDataFlow\/exercise.html'
        }
      ]
    },
    stats: {
      // Config for minimal console.log mess.
      assets: true,
      colors: true,
      version: true,
      hash: true,
      timings: true,
      chunks: false,
      chunkModules: false
    }
  }
}
