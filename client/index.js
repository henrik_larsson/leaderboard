import React from 'react'
import {render} from 'react-dom'
import Leaderboard from './components/Leaderboard'

render(
  <Leaderboard/>,
  document.getElementById('app')
)
