import React from 'react'
import Footer from './Footer'
import Menu from './Menu'

export default class Leaderboard extends React.Component {
  render() {
    return (
      <div>
        <Menu/>
        <h1>Leaderboard</h1>
        <div>Content goes here</div>
        <Footer/>
      </div>
    )
  }
}
