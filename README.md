## Leaderboard

To run:
```sh
$ git clone https://bitbucket.org/henrik_larsson/leaderboard.git
$ cd leaderboard
$ yarn install
$ yarn start
```

Then go to [http://localhost:8080](http://localhost:8080).
